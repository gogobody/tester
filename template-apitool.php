<?php
/*
Template Name: API测试工具
*/
if ( ! defined( 'ABSPATH' ) ) { exit; }
get_header('apitool'); ?>


<?php 
get_template_part( 'templates/sidebar','nav' );
?>
<div class="main-content flex-fill page">
<?php get_template_part( 'templates/header','banner' ); ?>
<div id="content" class="container my-3 my-md-4">
    <?php get_template_part( 'templates/breadcrumb' ); ?>
        <div id="app" v-cloak>
            <el-container>
                <el-header class="header">
                    <a href="/">
                        <img src="<?php _e(get_theme_file_uri('tester/static/images/logo.png')); ?>" height="80px" />
                    </a>
                    <span style="float:right;margin-bottom:10px;" class="no-select">
                    <el-link type="primary" :href="this.response.sharelink" target="_blank">分享API</el-link>
                    <el-link type="primary" href="https://www.ijkxs.com" target="_blank">查看博客</el-link>
                    <el-link type="primary" href="javascript:;" @click.native="dialogForSetting=true">系统设置</el-link>
                </span>
                </el-header>
                <el-main>
                    <div>
                        <el-autocomplete placeholder="请输入请求的URL" @input="requestUrlChanged" class="input-with-select no-select" v-model="request.url" :fetch-suggestions="querySearch" @select="handleSelect">
                            <el-link slot="prepend" class="type no-select" title="切换线上和本地版本" @click.native="changeType">{{nowType}}</el-link>
                            <el-select class="method no-select" v-model="request.method" slot="prepend" placeholder="请选择请求方式">
                                <el-option :label="item" :value="item" v-for="item in factory.methodList"></el-option>
                            </el-select>
                            <el-select slot="append" class="contentType no-select" v-model="factory.contentType" placeholder="ContentType" @change="contentTypeChanged">
                                <el-option :key="item.value" :label="item.label" :value="item.value" v-for="item in factory.contentTypeList"> <span style="float: left">{{ item.label }}　　</span>
                                    <span style="float: right; color: #8492a6; font-size: 13px">{{ item.value }}</span>

                                </el-option>
                            </el-select>
                            <el-button slot="append" icon="el-icon-s-promotion" @click="onSubmit" v-loading.fullscreen.lock="loading">请求</el-button>
                        </el-autocomplete>
                    </div>
                    <br>
                    <el-tabs type="border-card" v-model="factory.requestActive">
                        <el-tab-pane label="Body" name="Body">
                            <el-input type="textarea" rows="5" class="data" placeholder="a=b&c=d&#10;&#10;{'a':'b','c':'d'}&#10;&#10;xml" v-model="request.body"></el-input>
                        </el-tab-pane>
                        <el-tab-pane label="Header" name="Header">
                            <el-input type="textarea" rows="5" class="data" placeholder="User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36" v-model="request.header"></el-input>
                        </el-tab-pane>
                        <el-tab-pane label="Cookie" name="Cookie">
                            <el-input type="textarea" rows="5" class="data" v-model="request.cookie" placeholder="access_token=abcdefghijklmnopqrstuvwxyz;&#10;可直接复制Chrome控制台Set-Cookie的内容"></el-input>
                        </el-tab-pane>
                    </el-tabs>
                    <br>
                    <el-tabs type="border-card">
                        <el-tab-pane label="Body">
                            <pre v-html="response.body"></pre>
                        </el-tab-pane>
                        <el-tab-pane label="Preview" class="preview">
                            <iframe :srcdoc="escape2Html(response.body)"></iframe>
                        </el-tab-pane>
                        <el-tab-pane label="header">
                            <pre v-html="response.header"></pre>
                        </el-tab-pane>
                        <el-tab-pane label="Detail">
                            <pre v-html="response.detail"></pre>

                        </el-tab-pane>
                        <el-tab-pane label="MarkDown">
                            <el-link type="primary" href="https://md.hamm.cn" target="_blank" style="float:right;margin-bottom:10px;">MarkDown编辑器</el-link>
                            <el-input type="textarea" autosize placeholder="文档读取中" v-model="response.markdown"></el-input>
                        </el-tab-pane>
                    </el-tabs>
                </el-main>
            </el-container>
            <!-- 环境设置框 -->
            <el-dialog title="环境设置" :visible.sync="dialogForSetting" :modal-append-to-body='false'>
                <el-form status-icon>
                    <div class="tips">生成测试用例时会将本地地址替换为线上地址，切换调试环境时将会自动切换</div>
                    <el-form-item label="在线地址" label-width="80px">
                        <el-input size="medium" autocomplete="off" v-model="urlList.online" placeholder="在线域名 如https://tester.hamm.cn/"></el-input>
                    </el-form-item>
                    <el-form-item label="本地地址" label-width="80px">
                        <el-input size="medium" autocomplete="off" v-model="urlList.local" placeholder="本地域名 如http://127.0.0.1/"></el-input>
                    </el-form-item>
                </el-form>
                <div slot="footer" class="dialog-footer">
                    <el-button type="primary" @click="saveUrlList">配置完成</el-button>
                </div>
            </el-dialog>
        </div>
</div>
<?php get_footer('apitool'); ?>